# test-watermelon

Development of watermelon test.

## Running app

To run app with docker, in root project directory
```
docker-compose up --build
```

Then run the app on any browser:

http://localhost:8080/


If running with docker toolbox:

http://192.168.99.100:8080/

Note: 192.168.99.100 the default host. If you have other one configured you should use that instead.


## Running tests

To run tests and see coverages, in root project directory
```
cd restserver
npm install
npm run test-dev
```