const path = require('path');
require('./config/config');

module.exports = {
    entry: './src/app.js',
    output: {
    path: path.join(__dirname, 'public'),
        filename: 'bundle.js'
    },
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
                loader: 'babel-loader'
            }
        },
        {
            test: /\.css$/,
            loaders: ["style-loader", "css-loader"]
        },
        {
            test: /\.(jpg|png|svg)$/,
            use:{
                loader: 'file-loader'
            }
        }]
    },
    externals: {
        'Config': {
            apiurl: process.env.API_HOST
          }
    },
    devServer: {
        inline: true,
        host: process.env.HOST,
        contentBase: path.join(__dirname, 'public'),
        historyApiFallback: true
    }
};