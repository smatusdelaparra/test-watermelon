import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import Root from '../components/root';
import Characters from '../components/characters';

const AppRouter = () => (
    <BrowserRouter>
    <div>
        <Route path="/" component={Root} exact={true} />
        <Route path="/characters" component={Characters} />
    </div>
    </BrowserRouter>
);
export default AppRouter;