import React, {useState} from 'react';
import { Box, Container, AppBar, Tabs, Tab, Typography } from '@material-ui/core';
import Register from './register';
import Login from './login';
import logo from '../images/rick-morty.png';
import AssignmentIcon from '@material-ui/icons/Assignment';
import PersonIcon from '@material-ui/icons/Person';

function Root() {

    const [ selectedTab, setSelectedTab ] = useState(0);

    function handleTabChange(event, selectedTab) {
        setSelectedTab(selectedTab);
    };

    return (
        <div>
            <Container maxWidth="xs">
                <Box p={4}>
                    <img width="100%" src={logo} alt="logo" />
                </Box>
                <AppBar position="static">
                    <Tabs value={selectedTab} variant="fullWidth" onChange={handleTabChange} aria-label="simple tabs example">
                        <Tab icon={<AssignmentIcon />} label="Register" />
                        <Tab icon={<PersonIcon />}label="Sign in" />
                    </Tabs>
                </AppBar>
                    { selectedTab === 0 && <Register />} 
                    { selectedTab === 1 && <Login />}        
            </Container>
        </div>
    );
}

export default Root;