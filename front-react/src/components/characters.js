import React, { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import { Box, Card, CardActionArea, CardMedia, CardContent, Typography, Container, Grid } from '@material-ui/core';
import GridLoader from "react-spinners/ClipLoader";
import swal from 'sweetalert';
require('../../config/config');

const axios = require('axios');

function Characters() {

    const [ username, setUsername] = useState(localStorage.getItem('user'));
    const [ token, setToken ] = useState(localStorage.getItem('token'));
    const [ characters, setCharacters ] = useState([]);
    const [ loadingChars, setLoadingChars ] = useState(false);
    const [ redirectToLogin, setRedirectToLogin ] = useState(false);

    const spinnerStyle = { position: "fixed", top: "50%", left: "50%", transform: "translate(-50%, -50%)" };

    useEffect( () => {
        setLoadingChars(true);
        var config = {
            method: 'get',
            url: `http://${window.location.hostname}:${process.env.API_PORT}/rickAndMortyCharacters`,
            headers: { 
              token
            }
        };
        axios(config).then((response) => {
            setLoadingChars(false);
            setCharacters(response.data);
        })
        .catch((error) => {
            swal({
                title: error.response.data.err.message,
                text: 'Redirecting home...',
                icon: "error",
              });
            setLoadingChars(false);
            setRedirectToLogin(true);
        });
    }, []);

    return (
        <div>
            {redirectToLogin ? <Redirect to='/'/> : null }

            <Box m={10}>
            <Grid container spacing={3}>
            {
                loadingChars ? 
                <div style={spinnerStyle} className="sweet-loading">
                    <GridLoader size={150} color={"#123abc"} loading={loadingChars} />
                </div> 
                :
                characters.map((character, index) =>
                    <Grid key={index} item xs={2}>
                        <Card>
                            <CardActionArea>
                                <CardMedia component="img" image={character.image} title={character.name} />
                                <CardContent>
                                    <Typography gutterBottom variant="h6" component="h2">
                                        {character.name}
                                    </Typography>
                                    <Typography variant="body2" color="textSecondary" component="p">
                                        Species: {character.species} 
                                    </Typography>
                                    <Typography variant="body2" color="textSecondary" component="p">
                                        Gender: {character.gender} 
                                    </Typography>
                                    <Typography variant="body2" color="textSecondary" component="p">
                                        Status: {character.status} 
                                    </Typography>
                                </CardContent>
                            </CardActionArea>
                        </Card>
                    </Grid>
                )
            }
            </Grid>
            </Box>
        </div>
    );
}

export default Characters;