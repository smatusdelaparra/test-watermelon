import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import swal from 'sweetalert';
require('../../config/config');

import { Box, TextField, Button, Typography } from '@material-ui/core';

const axios = require('axios');
const qs = require('qs');

function Login() {

    const [ username, setUsername ] = useState('');
    const [ password, setPassword ] = useState('');
    const [ redirectToChars, setRedirectToChars ] = useState(false);

    function loginService(username, password){
        var data = qs.stringify({
            email: username,
            password
        });
        var config = {
            method: 'post',
            url: `http://${window.location.hostname}:${process.env.API_PORT}/login`,
            headers: { 
              'Content-Type': 'application/x-www-form-urlencoded'
            },
            data : data
        };
        axios(config).then((response) => {
            if(response.data.auth){
                localStorage.setItem('user', response.data.user);
                localStorage.setItem('token', response.data.token);
                setRedirectToChars(true);
            }
        })
        .catch((error) => {
            const text = error.response ? error.response.data.err.message : "Can't connect to server";
            swal({
                title: 'Error',
                text,
                icon: "error",
              });
        });
    };

    function handleSubmit(e){
        e.preventDefault();

        if (!(username && password)) {
            return;
        }

        loginService(username,password);
    };

    return (
        <div>
            {redirectToChars ? <Redirect to='/characters'/> : null }
            <Box p={3}>
                <Typography align="center" component="h2" variant="h5">
                    Sign in and enjoy !
                </Typography>
            </Box>
            <form onSubmit={handleSubmit} noValidate autoComplete="off">
                <TextField required id="login-username" onChange={ e => setUsername(e.target.value)} margin="normal" fullWidth label="username" name="username" variant="outlined" />
                <TextField required id="login-password" onChange={e => setPassword(e.target.value)} margin="normal" fullWidth label="password" type="password" name="password" variant="outlined" />
                <Button type="submit" fullWidth variant="contained" color="primary">
                    Sign In
                </Button>
            </form>
        </div>
    );
}

export default Login;