import React, {useState} from 'react';
const axios = require('axios');
const qs = require('qs');

import { Box, TextField, Button, Typography } from '@material-ui/core';

function Register() {
    
    const [ username, setUsername ] = useState('');
    const [ password, setPassword ] = useState('');

    function registerService(email, password){
        var data = qs.stringify({
            email,
            password
        });
        var config = {
            method: 'post',
            url: `http://${window.location.hostname}:${process.env.API_PORT}/register`,
            headers: { 
              'Content-Type': 'application/x-www-form-urlencoded'
            },
            data : data
        };
        axios(config).then((response) => {
            if(response.data.registered){
                swal({
                    title: 'Registrado',
                    icon: "success",
                  });
            }
        })
        .catch(function (error) {
            const text = error.response ? error.response.data.err.message : "Can't connect to server";
            swal({
                title: 'Error',
                text,
                icon: "error",
              });
        });
    }

    function handleSubmit(e){
        e.preventDefault();

        if (!(username && password)) {
            return;
        }

        registerService(username, password);
    }

    return (
        <div>
            <Box p={3}>
                <Typography  align="center" component="h2" variant="h5">
                    Register to see characters
                </Typography>
            </Box>
                
            <form onSubmit={handleSubmit} noValidate autoComplete="off">
                <TextField required id="register-username" onChange={e=>setUsername(e.target.value)} margin="normal" fullWidth label="username" name="username" variant="outlined" />
                <TextField required id="register-password" onChange={e=>setPassword(e.target.value)} margin="normal" fullWidth label="password" type="password" name="password" variant="outlined" />
                <Button type="submit" fullWidth variant="contained" color="primary" >
                    Sign Up
                </Button>
            </form>
        </div>
    );
}

export default Register;