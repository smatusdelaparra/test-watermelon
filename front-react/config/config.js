// App host
process.env.HOST = process.env.HOST || '127.0.0.1';
process.env.PORT = process.env.PORT || 8080;

// Backend port
process.env.API_PORT = process.env.API_PORT || 3000;

// Env
process.env.REACT_ENV = process.env.REACT_ENV || 'dev';