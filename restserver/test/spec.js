const redis = require('redis');
const redis_mock = require('redis-mock');
jest.spyOn(redis, 'createClient').mockImplementation(redis_mock.createClient)


const UserRepository = require('../src/repository/userRepository.js');
const CharacterRepository = require('../src/repository/characterRepository.js');

const defaultCharactersFromApi = require('./defaultApiResponse');


/* ======= TEST UNITARIOS REPOSITORY ======= */

test('Should add user', (done) => {
    expect(UserRepository.addUser('testuser', 'testpassword', (userAdded) => {
        expect(userAdded).toBe(true);
        done();
    }));
});

test('Should not add user', (done) => {
    expect(UserRepository.addUser('testuser', 'testpassword', (userAdded) => {
        expect(userAdded).toBe(false);
        done();
    }));
});

test('Should get user', (done) => {
    expect(UserRepository.getUserByEmail('testuser', (user) => {
        expect(user).toEqual({
            email: 'testuser', 
            password: 'testpassword'
        });
        done();
    }));
});

test('Should not get user', (done) => {
    expect(UserRepository.getUserByEmail('nonExistingUser', (user) => {
        expect(user).toBe(null);
        done();
    }));
});


test('Should get characters from api', (done) => {
    expect(CharacterRepository.getCharacters( (characterList) => {
        expect(characterList).toEqual(defaultCharactersFromApi);
        done();
    }));
});

/* ======= END TEST UNITARIOS REPOSITORY ======= */







/* ======= TEST API ENDPOINTS  ======= */ 

/* Supertest pre configuration */
require('../src/config/config');
const Server = require('../src/server');
const host = process.env.HOST;
const port = process.env.PORT;
const serverInstance = new Server(host, port);
serverInstance.startServer();
const app = serverInstance.getExpressInstance();

const supertest = require('supertest');
const request = supertest(app);


/* Variable para guardar token de un test login y probar en test getcharacters */
var tokenForCharTest;


/* TESTS */
test('Should add user API', async done => {
    const response = await request.post('/register')
        .send({
            email: 'testemail2@test.com',
            password: 'testpassword'
        })
    expect(response.status).toBe(200);
    done();
});

test('Should not add user API', async done => {
    const response = await request.post('/register')
        .send({
            email: 'testemail2@test.com',
            password: 'testpassword'
        })
    expect(response.status).toBe(400);
    done();
});

test('Should login API', async done => {
    const response = await request.post('/login')
        .send({
            email: 'testemail2@test.com',
            password: 'testpassword'
        })
    expect(response.status).toBe(200);
    tokenForCharTest = response.body.token;
    done();
});


test('Should be jwt malformed', async done => {
    const response = await request.get('/rickAndMortyCharacters')
    .set('token', 'badToken');
    expect(response.status).toBe(401);
    done();
});


test('Should get Rick and Morty characters', async done => {
    const response = await request.get('/rickAndMortyCharacters')
    .set('token', tokenForCharTest);
    expect(response.status).toBe(200);
    expect(response.body).toEqual(defaultCharactersFromApi);
    done();
});


