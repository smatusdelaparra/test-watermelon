require('./config/config');
const Server = require('./server');

const host = process.env.HOST;
const port = process.env.PORT;

const app = new Server(host, port);
app.startServer();

module.exports = app;