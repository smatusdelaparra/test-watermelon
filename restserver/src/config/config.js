// App host
process.env.HOST = process.env.HOST || '127.0.0.1';
process.env.PORT = process.env.PORT || 3000;

//Redis host
process.env.REDIS_URL = process.env.REDIS_URL || '127.0.0.1';
process.env.REDIS_PORT = process.env.REDIS_PORT || 6379;

// Env
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';

// Token properties
process.env.TOKEN_EXP_TIME = process.env.TOKEN_EXP_TIME || '2m';
process.env.TOKEN_SECRET = process.env.TOKEN_SECRET || 'secret-dev';

// Rick and morty api endpoint
process.env.API_ENDPOINT = process.env.API_ENDPOINT || 'https://rickandmortyapi.com/api/character/';