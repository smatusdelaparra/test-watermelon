const request = require('request');


class CharacterRepository{

    static getCharacters(characterList) {
        var options = {
            'method': 'GET',
            'url': process.env.API_ENDPOINT,
            'headers': {}
        };
        request(options, (error, response) => {
            if (error) throw new Error(error);
            else{
                var charactersFromResponse = JSON.parse(response.body).results;
                var charactersWithFilter = charactersFromResponse.map(x => {
                    return {
                        name: x.name,
                        status: x.status,
                        species: x.species,
                        gender: x.gender,
                        image: x.image
                    }
                });
                characterList(charactersWithFilter);
            }
        });
    }
}


module.exports = CharacterRepository;