const redis = require('redis');


class AuthRepository {

    constructor(){
        this.client = redis.createClient(process.env.REDIS_PORT ,process.env.REDIS_URL);

        this.client.on('connect', () => {
            console.log('Connected to redis!');
        });
        
        this.client.on('error', () => {
            console.log('Error connecting to redis!');
        });
    }

    getUserByEmail(email, returningUser){
        this.client.hmget(`email:${email}`, 'password', (err, obj) => {
            if(err) throw err;
            if (obj[0]) {
                return returningUser({
                    email,
                    password: obj[0]
                });
            }
            return returningUser(null);
        });
    }
    
    addUser(email, encryptedPassword, userAdded){
        this.client.hset(`email:${email}`, 'password', encryptedPassword, (err, result) => {
            if(err) throw err;
            if(result===1){
                return userAdded(true);
            }else{
                return userAdded(false);
            }
            
        });
    }
}


module.exports = new AuthRepository();