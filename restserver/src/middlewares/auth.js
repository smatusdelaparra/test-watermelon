const jwt = require('jsonwebtoken');

class AuthMiddleware {

    static validateToken(req, res, next){
        var token = req.get('token');
    
        jwt.verify(token, process.env.TOKEN_SECRET, (err, decoded) => {
            if (err) {
                console.log(`Error: ${err}`);
                return res.status(401).json({
                    err
                })
            }
            next();
        })
    }

    static async validateMandatoryFields(req, res, next){
        /* Caso bad request */
        const body = req.body;
        if(!(body.email && body.password)){
            return res.status(400).json({
                registered: false,
                err: {
                    message: 'User and password mandatory'
                }
            });
        }else{
            next();
        }
    }
}


module.exports = AuthMiddleware;