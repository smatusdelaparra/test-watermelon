const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const UserRepository = require('../repository/userRepository');


class AuthController {

    static async signUp(req, res) {
        var body = req.body;
        
        try {
            UserRepository.getUserByEmail(body.email, (userFromRedis) => {
                if(!userFromRedis){
                    UserRepository.addUser(body.email, bcrypt.hashSync(body.password, 10), (userAdded) => {
                        if(userAdded){
                            /* Caso OK */
                            return res.status(200).json({
                                registered: true,
                                user: body
                            });
                        }
                    });
                }else{
                    return res.status(400).json({
                        registered: false,
                        err: {
                            message: 'User already exists'
                        }
                    });
                }
            });
        } catch(err){
            console.log(`Error: ${err}`);
            return res.status(500).json({
                registered: false,
                err
            });
        }
    }


    static async login(req, res) {
        var body = req.body;

        try {
            UserRepository.getUserByEmail(body.email, (userFromRedis) => {
                if(userFromRedis){
                    /* Caso contrasenha no match */
                    if (!bcrypt.compareSync(body.password, userFromRedis.password)) {
                        return res.status(400).json({
                            auth: false,
                            err: {
                                message: 'Invalid password'
                            }
                        });
                    }
                    var token = jwt.sign({
                        usuario: body.email
                    }, process.env.TOKEN_SECRET, { expiresIn: process.env.TOKEN_EXP_TIME });
            
                    /* Caso OK */
                    res.status(200).json({
                        auth: true,
                        user: body.email,
                        token
                    });
                }else{
                    return res.status(400).json({
                        auth: false,
                        err: {
                            message: 'Invalid user'
                        }
                    });
                }
            });
        } catch(err){
            console.log(`Error: ${err}`);
            return res.status(500).json({
                registered: false,
                err
            });
        }
    }
}



module.exports = AuthController;