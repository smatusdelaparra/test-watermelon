const CharacterRepository = require('../repository/characterRepository');


class CharacterController {

    static async getCharacters(req, res){
        try{
            CharacterRepository.getCharacters( characterList => {
                if(characterList){
                    return res.status(200).json(characterList);
                }else{
                    return res.status(404).json({
                        err: {
                            message: 'No character found'
                        }
                    });
                }
            });
        }catch(err){
            return res.status(500).json({
                err
            })
        }
    }
}


module.exports = CharacterController;