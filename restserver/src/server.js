const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const authRouter = require('./routes/auth');
const characterRouter = require('./routes/characters');


class Server {

    constructor(host, port){
        this.host = host;
        this.port = port;
        this.app = express();
    }
    
    config(){
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(bodyParser.json());
        this.app.use(cors());
        this.app.use(authRouter);
        this.app.use(characterRouter);
    }

    startServer(){
        this.config();
        this.app.listen(this.port, this.host, () => console.log(`App iniciada: ${this.host}:${this.port}`));
    }

    getExpressInstance(){
        return this.app;
    }

}

module.exports = Server;