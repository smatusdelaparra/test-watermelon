const express = require('express');
const router = express.Router();

const AuthMiddleware = require('../middlewares/auth');
const CharacterController = require('../controllers/characters');


router.get('/rickAndMortyCharacters', AuthMiddleware.validateToken, CharacterController.getCharacters);


module.exports = router;