const express = require('express');
const router = express.Router();

const AuthMiddleware = require('../middlewares/auth');
const AuthController = require('../controllers/auth');


router.post('/register', AuthMiddleware.validateMandatoryFields, AuthController.signUp);
router.post('/login', AuthMiddleware.validateMandatoryFields, AuthController.login)


module.exports = router;